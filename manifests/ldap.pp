class freebsd::ldap {
  package {
    [
      'nss-pam-ldapd',
      'openldap-client',
    ]:
      ensure => installed,
  }

  file { '/etc/nsswitch.conf':
    ensure => file,
    source => 'puppet:///modules/freebsd/ldap/nsswitch.conf',
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
  }

  file { '/etc/krb5.conf':
    ensure => file,
    source => 'puppet:///modules/freebsd/ldap/krb5.conf',
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
  }

  file { '/etc/pam.d/sshd':
    ensure => file,
    source => 'puppet:///modules/freebsd/ldap/pam.d/sshd',
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
  }

  file { '/etc/pam.d/system':
    ensure => file,
    source => 'puppet:///modules/freebsd/ldap/pam.d/system',
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
  }

  file { '/etc/pam.d/su':
    ensure => file,
    source => 'puppet:///modules/freebsd/ldap/pam.d/su',
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
  }

  file { '/etc/auto_master':
    ensure => file,
    source => 'puppet:///modules/freebsd/ldap/autofs/auto_master',
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
  }

  file { '/etc/autofs/include_ldap':
    ensure => file,
    source => 'puppet:///modules/freebsd/ldap/autofs/include_ldap',
    owner  => 'root',
    group  => 'wheel',
    mode   => '0755',
  }

  file { '/etc/autofs/include':
    ensure => 'link',
    target => '/etc/autofs/include_ldap',
  }

  file_line { 'Enable autofs':
    path => '/etc/rc.conf',
    line => 'autofs_enable="YES"',
  }

  file { '/usr/local/etc/nslcd.conf':
    ensure => file,
    source => 'puppet:///modules/freebsd/ldap/nslcd.conf',
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
  }

  file_line { 'Enable nslcd':
    path => '/etc/rc.conf',
    line => 'nslcd_enable="YES"',
  }

  file { '/usr/local/etc/openldap/ldap.conf':
    ensure => file,
    source => 'puppet:///modules/freebsd/ldap/openldap/ldap.conf',
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
  }

  file { '/usr/local/etc/ipa':
    ensure => directory,
  }

  file { '/usr/local/etc/ipa/ca.crt':
    ensure => file,
    source => 'puppet:///modules/freebsd/ldap/ipa/ca.crt',
    owner  => 'root',
    group  => 'wheel',
    mode   => '0644',
  }
}
