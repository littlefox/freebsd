#
class freebsd::firewall::open {
  file_line { 'Enable firewall':
    path => '/etc/rc.conf',
    line => 'firewall_enable="YES"',
  }
  file_line { 'Set type to open':
    path => '/etc/rc.conf',
    line => 'firewall_type="open"',
  }
}
